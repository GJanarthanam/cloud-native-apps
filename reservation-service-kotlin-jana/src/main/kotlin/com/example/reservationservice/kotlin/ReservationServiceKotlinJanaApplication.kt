package com.example.reservationservice.kotlin

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

interface ReservationRepo : JpaRepository<Reservation, Long>

@Entity
class Reservation(@Id @GeneratedValue val id: Long = 0, val name: String?) {
    constructor() : this(0, null)
}

@RestController
@RefreshScope
class MessageRestControl(@Value("\${message}") val value: String) {

    @GetMapping("/message")
    fun read() = value
}

@SpringBootApplication
@EnableEurekaClient
class ReservationServiceKotlinJanaApplication(val reservationRepo: ReservationRepo) : CommandLineRunner {

    override fun run(vararg args: String?) {
        listOf("JFK", "LBJ", "MLK")
                .map { it -> reservationRepo.save(Reservation(name = it)) }
        reservationRepo.findAll()
                .map { println(it.name) }
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(ReservationServiceKotlinJanaApplication::class.java, *args)
}

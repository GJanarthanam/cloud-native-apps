package com.example.eurekaclient2.eurekaclient2;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@RestController
@EnableHystrix
@EnableHystrixDashboard
public class ServiceDiscoveryClient {

  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }

  @Autowired private RestTemplate restTemplate;

  @Autowired EurekaClient client;

  @GetMapping("/discovery")
  @HystrixCommand(
    fallbackMethod = "failover",
    commandProperties = {
      @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "500")
    }
  )
  public String mgiProductCatalogServiceInfo(@RequestParam long time) throws Exception {
    Thread.sleep(time);
    InstanceInfo instance = client.getNextServerFromEureka("MGI_Product_Catalog_Service", false);
    return "Service Discovery - MGI_Product_Catalog_Service URL: " + instance.getHomePageUrl();
  }

  public String failover(long time) {
    return "Hystrix: this message is coming from failover method";
  }

  public static void main(String[] args) {
    SpringApplication.run(ServiceDiscoveryClient.class, args);
  }
}

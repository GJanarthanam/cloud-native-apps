package com.example.eurekaclient.eurekaclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@RestController
public class ExampleController {

  @Autowired private RestTemplate restTemplate;

  @RequestMapping("/execute")
  public String execute() {
    return this.restTemplate.getForObject("http://jana-eureka-client2/serviceinfo", String.class);
  }
}

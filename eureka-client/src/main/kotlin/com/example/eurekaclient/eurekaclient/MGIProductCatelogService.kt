package com.example.eurekaclient.eurekaclient

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.loadbalancer.LoadBalanced
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.context.annotation.Bean
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import java.util.*

@SpringBootApplication
@EnableEurekaClient
@RestController
class MGIProductCatalogService {

    @Bean
    @LoadBalanced
    fun restTemplate(): RestTemplate {
        return RestTemplate()
    }

    @RequestMapping("/products")
    fun products(): List<String> {
        return Arrays.asList("Money Transfer", "Money Order", "Bill Payment")
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(MGIProductCatalogService::class.java, *args)


}

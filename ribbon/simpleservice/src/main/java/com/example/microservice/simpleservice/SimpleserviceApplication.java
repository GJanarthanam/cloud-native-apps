package com.example.microservice.simpleservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SimpleserviceApplication {

    @Value("${server.port}")
    public String port;

    @GetMapping("/")
    public String status(){
        return "UP";
    }

    @GetMapping("/execute")
    public String execute(){
        return "Hello from the port: " + this.port;
    }

	public static void main(String[] args) {
		SpringApplication.run(SimpleserviceApplication.class, args);
	}
}
